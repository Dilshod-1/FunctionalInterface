package docStream;

public class doc5 {
    @FunctionalInterface
    interface Project {
            String doString(String s1, String s2);
    }

    public static void main(String[] args) {
        Project oux = (String s1,String s2) -> {
            if(s1.length()>s2.length()) {
                return s1;
            }else{
                return s2;
            }
        };
        String result = oux.doString("Dilshod","Dil");
        System.out.println(result);
    }
}
